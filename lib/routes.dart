import 'package:flutter/material.dart';
import 'screens/chat_screen.dart';
import 'screens/login_screen.dart';
import 'screens/registration_screen.dart';
import 'screens/welcome_screen.dart';

abstract class Enum<T> {
  final T value;
  const Enum(this.value);
}

//class Route<String> extends Enum<String> {
//  const Route(String val) : super(val);
//
//  static const Route WELCOME = const Route('welecome');
//  static const Route CHAT = const Route('chat');
//  static const Route LOGIN = const Route('login');
//  static const Route REGISTER = const Route('register');
//}

//class RouteManager {
//  static void push(Route route, BuildContext context) {
//    Navigator.pushNamed(context, route.value);
//  }
//}

//Map<String, WidgetBuilder> kRoutes = {
//  WelcomeScreen.id: (context) => WelcomeScreen(),
//  RegistrationScreen.id: (context) => RegistrationScreen(),
//  LoginScreen.id: (context) => LoginScreen(),
//  ChatScreen.id: (context) => ChatScreen(),
//};
